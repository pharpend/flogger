module Handler.PostView where

import Import

getPostViewR :: PostId -> Handler Html
getPostViewR ident = do 
  (Post ttl dte mtxt) <- runDB $ get404 ident
  defaultLayout $ do
    setTitle $ toHtml ttl
    [whamlet|
      <h1>#{ttl}
      <p>Published on #{show dte}
      $maybe t <- mtxt
        #{t}
      $nothing
    |]
