module Handler.PostNew where

import Data.Time (getCurrentTime)
import Import

entryForm :: Html -> MForm Handler (FormResult Post, Widget)
entryForm = 
  renderBootstrap $ Post
   <$> areq textField "Title" Nothing
   <*> lift (liftIO getCurrentTime)
   <*> aopt textareaField "Text" Nothing

getPostNewR :: Handler Html
getPostNewR = do
  (wdg, _) <- generateFormPost entryForm
  defaultLayout [whamlet|
                <form method=post action=@{PostNewR}>
                  ^{wdg}
                  <input type=submit>
                |]

postPostNewR :: Handler Html
postPostNewR = do
  ((result, widget), _) <- runFormPost entryForm
  case result of
    FormSuccess p -> do
      ident <- runDB $ insert p
      redirect $ PostViewR ident
    _ -> 
      defaultLayout [whamlet|
                    <p> Something got fucked up, try again.
                    <form method=post action=@{PostNewR}>
                      ^{widget}
                      <input type=submit>
                    |]
